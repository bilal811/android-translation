package com.massarttech.android.translation;

import org.junit.Test;

import java.util.Locale;

import static org.junit.Assert.assertEquals;


public class TranslationOptionsTest {

    @Test
    public void autoDetectLang() {
        TranslationOptions builder = TranslationOptions.newBuilder("mian g")
                .addLanguage(Language.ENGLISH)
                .addLanguage(Language.ARABIC)
                .addLanguage(Language.URDU)
                .autoDetectLanguage()
                .build();
        assertEquals(Locale.getDefault().getLanguage(), builder.getCurrentLanguage().getLangCode());
    }

    /**
     * Do not repeat languages in language even if user calls to
     * {@link com.massarttech.android.translation.TranslationOptions.Builder#addLanguage(Language)}
     * with same english many time
     */
    @Test
    public void repeatedLanguagesTest() {
        TranslationOptions builder = TranslationOptions.newBuilder("mian g")
                .addLanguage(Language.ENGLISH)
                .addLanguage(Language.ENGLISH)
                .addLanguage(Language.ARABIC)
                .addLanguage(Language.URDU)
                .autoDetectLanguage()
                .build();
        assertEquals(builder.getLanguages().size(), 3);
    }

    /**
     * When building {@link TranslationOptions} one must set string calls using
     * {@link TranslationOptions#baseUrl}
     */
    @Test
    public void stingBaseUrlNotNull() {
        TranslationOptions builder = TranslationOptions.newBuilder("mian g")
                .addLanguage(Language.ENGLISH)
                .addLanguage(Language.ARABIC)
                .addLanguage(Language.URDU)
                .autoDetectLanguage()
                .build();
        assert builder.getBaseUrl() != null;
    }

    /**
     * If user calls both {@link TranslationOptions.Builder#autoDetectLanguage()} and
     * {@link com.massarttech.android.translation.TranslationOptions.Builder#currentLanguage(Language)}
     * then auto detect must not set language.
     */
    @Test
    public void currentLanguageReplaceAutoDetect() {
        TranslationOptions builder = TranslationOptions.newBuilder("mian g")
                .addLanguage(Language.ENGLISH)
                .addLanguage(Language.ENGLISH)
                .addLanguage(Language.ARABIC)
                .addLanguage(Language.URDU)
                .currentLanguage(Language.URDU)
                .autoDetectLanguage()
                .build();
        assertEquals(builder.getCurrentLanguage(), Language.URDU);
    }
}