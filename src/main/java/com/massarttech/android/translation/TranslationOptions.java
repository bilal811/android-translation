package com.massarttech.android.translation;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class TranslationOptions {
    private List<Language> languages;
    private Language currentLanguage;
    private final String baseUrl;

    public TranslationOptions(String baseUrl) {
        this.baseUrl = baseUrl;
    }


    public static Builder newBuilder(@NonNull String baseUrl) {
        return new Builder(baseUrl);
    }

    public String getBaseUrl() {
        return baseUrl;
    }


    public Language getCurrentLanguage() {
        return currentLanguage;
    }

    public void setCurrentLanguage(Language currentLanguage) {
        this.currentLanguage = currentLanguage;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public static class Builder {
        private final TranslationOptions options;
        private boolean autoDetectLang;

        private Builder(@NonNull String baseUrl) {
            this.options = new TranslationOptions(baseUrl);
            this.options.languages = new ArrayList<>();
        }


        public Builder autoDetectLanguage() {
            this.autoDetectLang = true;
            return this;
        }

        public Builder addLanguage(@NonNull Language language) {
            if (options.languages.contains(language)) {
                return this;
            }
            options.languages.add(language);
            return this;
        }


        public Builder currentLanguage(@NonNull Language language) {
            this.options.currentLanguage = language;
            return this;
        }

        @NonNull
        public TranslationOptions build() {
            if (autoDetectLang && options.currentLanguage == null) {
                setAutoLanguage();
            }
            if (!options.languages.contains(options.currentLanguage)) {
                throw new IllegalArgumentException("Current language does not exist in translated languages.");

            }
            if (options.baseUrl == null) {
                throw new IllegalArgumentException("You must provide base url");
            }


            return options;
        }

        private void setAutoLanguage() {
            String lang = Locale.getDefault().getLanguage();
            try {
                this.options.currentLanguage = Language.fromString(lang);
            } catch (IllegalArgumentException e) {
                this.options.currentLanguage = Language.ENGLISH;
            }
        }
    }

}
