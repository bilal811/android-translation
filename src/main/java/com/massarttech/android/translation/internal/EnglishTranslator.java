package com.massarttech.android.translation.internal;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

import com.massarttech.android.translation.domain.Translation;
import com.massarttech.android.translation.internal.data.DataHolder;

@RestrictTo(RestrictTo.Scope.LIBRARY)
public class EnglishTranslator extends Translator {
    public EnglishTranslator(@NonNull DataHolder dataHolder) {
        super(dataHolder);
    }

    @Override
    @NonNull
    public String getTranslation(@NonNull Context context, @NonNull String screenName, @NonNull String key, Object... params) {
        Translation translation = dataHolder.getTranslation(screenName, key);
        if (translation != null) {
            String text = translation.getEnglish();
            if (text == null) {
                text = translation.getOriginal();
                if (text == null) {
                    return super.getTranslation(context, screenName, key, params);
                }
            }
            if (params != null && params.length > 0) {
                try {
                    return String.format(text, params);
                } catch (Exception e) {
                    return text;
                }
            }
            return text;
        }
        return super.getTranslation(context, screenName, key, params);
    }

}
