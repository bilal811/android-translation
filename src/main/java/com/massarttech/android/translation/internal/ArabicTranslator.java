package com.massarttech.android.translation.internal;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

import com.massarttech.android.translation.domain.Translation;
import com.massarttech.android.translation.internal.data.DataHolder;

@RestrictTo(RestrictTo.Scope.LIBRARY)
public class ArabicTranslator extends Translator {
    public ArabicTranslator(@NonNull DataHolder dataHolder) {
        super(dataHolder);
    }

    @NonNull
    @Override
    public String getTranslation(@NonNull Context context, @NonNull String screenName, @NonNull String key, Object... params) {
        Translation translation = dataHolder.getTranslation(screenName, key);
        if (translation != null) {
            String text = translation.getArabic();
            if (text == null) {
                text = translation.getOriginal();
                if (text == null) {
                    return super.getTranslation(context, screenName, key, params);
                }
            }
            if (params != null && params.length > 0) {
                try {
                    return String.format(text.replace("٪ ", "٪")
                            .replace(" ٪","٪")
                            .replace("٪", "%"), params);
                } catch (Exception e) {
                    return text;
                }
            }
            return text;
        }
        return super.getTranslation(context, screenName, key, params);
    }

}
