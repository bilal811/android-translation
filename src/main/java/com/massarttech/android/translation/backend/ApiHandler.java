package com.massarttech.android.translation.backend;

import androidx.annotation.NonNull;

import com.massarttech.android.translation.domain.Translation;

import java.util.HashMap;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiHandler {
    private static ApiHandler instance;
    private final EndPoints endPoints;

    private ApiHandler(@NonNull String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        endPoints = retrofit.create(EndPoints.class);
    }

    public static ApiHandler getInstance(@NonNull String baseUrl) {
        if (instance == null) {
            instance = new ApiHandler(baseUrl);
        }
        return instance;
    }


    public void loadAppData(@NonNull OnApiResponse<HashMap<String, HashMap<String, Translation>>> completion) {
        endPoints.getTranslations().enqueue(new Callback<>(completion));
    }
}
