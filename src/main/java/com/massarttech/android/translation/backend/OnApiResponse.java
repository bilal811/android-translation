package com.massarttech.android.translation.backend;

import androidx.annotation.NonNull;

/**
 * Created by bullhead on 2/7/18.
 */

public interface OnApiResponse<T> {
    void onResponse();

    void onSuccess(@NonNull T response);

    void onError();
}
