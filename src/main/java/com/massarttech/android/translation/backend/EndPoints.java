package com.massarttech.android.translation.backend;

import com.massarttech.android.translation.domain.Translation;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;

public interface EndPoints {
    @GET("translations/get")
    Call<HashMap<String, HashMap<String, Translation>>> getTranslations();
}
